class ObjectPool
    generate = (instance, amount) ->
        for _ = 1, amount
            obj = instance.generator_func!
            instance.inactive_objects[obj] = obj

    expand = (instance) ->
        generate instance, instance.size
        instance.size *= 2
        next instance.inactive_objects

    fallible = ->

    cycle = (instance) ->
        return next(instance.active_objects, instance.previous_overflow_entity)

    @OverflowBehaviors: {
        :expand,
        :fallible,
        :cycle
    }

    empty_generator = -> return {}

    new: (size = 16, generator_func = empty_generator, overflow_behavior = expand, parent) =>
        @size = size
        @inactive_objects = {}
        @active_objects = {}
        @generator_func = generator_func
        @overflow_behavior = overflow_behavior
        @parent = parent -- used for hooking into overflow behavior

        generate @, size

    get_inactive_object = =>
        obj = next(@inactive_objects)
        if obj == nil then
            obj = @overflow_behavior @parent
            @previous_overflow_obj = obj
        obj

    -- warning: create can return nil!
    obtain: =>
        obj = get_inactive_object @
        if obj != nil then
           @inactive_objects[obj] = nil
           @active_objects[obj] = obj
        return obj

    free: (obj) =>
        assert @active_objects[obj] != nil, "cannot free inactive object"
        @active_objects[obj] = nil
        @inactive_objects[obj] = obj

return ObjectPool