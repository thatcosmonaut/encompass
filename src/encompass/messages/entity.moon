up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

Entity = require(up_one_folder .. 'entity')
Message = require(up_one_folder .. 'message')

return Message\define("ComponentMessage", { entity: Entity })