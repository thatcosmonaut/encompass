utils = {}

utils.empty = (t) ->
    next(t) == nil

utils.clear = (t) ->
    for k in pairs t
        t[k] = nil

__gen_ordered_index = (t) ->
    ordered_index = {}
    for k in pairs t
        table.insert ordered_index, k
    table.sort ordered_index
    ordered_index

ordered_next = (t, state) ->
    key = nil
    if state == nil
        t.__ordered_index = __gen_ordered_index t
        key = t.__ordered_index[1]
    else
        for i = 1, table.getn t.__ordered_index
            if t.__ordered_index[i] == state
                key = t.__ordered_index[i + 1]

    if key then return key, t[key]

    t.__ordered_index = nil
    return

utils.ordered_pairs = (t) ->
    ordered_next, t, nil

utils.isarray = (x) ->
    (type(x) == "table" and x[1] ~= nil) and true or false

getiter = (x) ->
    if utils.isarray x
        ipairs
    elseif type(x) == "table"
        pairs
    else
        error "expected table", 3

utils.remove = (t, x) ->
    iter = getiter t
    for i, v in iter t
        if v == x
            if utils.isarray t
                table.remove t, i
                break
            else
                t[i] = nil
                break
    x

utils.lookup_chain = (t, v, ...) ->
    if v == nil then return t

    t = t[v]

    if not t
        return nil
    else
        return utils.lookup_chain t, ...

supertype = (t1, t2) ->
    if type(t1) == "table"
        return t1 == t2 or supertype(t1.__parent, t2)
    t1 == t2

utils.encompasstype = (var, _type) ->
    if type(var) == "table" and var.__class ~= nil
        var.__class == _type or supertype(var.__class.__parent, _type)
    else
        type(var) == _type

utils.callable = (var) ->
    type(var) == "function" or (getmetatable(var) ~= nil and type(getmetatable(var).__call) == "function")

return utils