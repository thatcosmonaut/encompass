up_two_folders = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'
current_folder = (...)\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_two_folders .. 'utils')
ObjectPool = require(up_two_folders .. 'object_pool')
Spawner = require(current_folder .. 'spawner')

class PooledSpawner extends Spawner
    @frozen: true

    cycle = (object_pool, pooled_spawner) ->
        entity = next(object_pool.active_objects, object_pool.previous_overflow_entity)
        pooled_spawner.world\__clear_messages_for_entity entity
        entity

    @OverflowBehaviors: {
        expand: ObjectPool.OverflowBehaviors.expand,
        fallible: ObjectPool.OverflowBehaviors.fallible,
        cycle: cycle
    }

    @__inherited: (subklass) =>
        subklass.frozen = false
        subklass\assert_has_message_type!
        assert subklass.pool_size != nil and type(subklass.pool_size) == 'number', subklass.__name .. " must have a pool_size"
        assert subklass.generate != nil and utils.callable(subklass.generate), subklass.__name .. " must implement generate function"
        assert subklass.spawn != nil and utils.callable(subklass.generate), subklass.__name .. " must implement spawn function"

    new: (world, ...) =>
        super world, ...
        @object_pool = ObjectPool @@pool_size, ->
            entity = @world\__create_pooled_entity(@)
            entity.spawner = @
            @generate entity
            entity,
            @@overflow_behavior,
            @

    __activate: =>
        @object_pool\obtain!

    __deactivate: (entity) =>
        @object_pool\free entity

    __update: =>
        for _, message in pairs @messages
            entity = @object_pool\obtain!
            if entity != nil then
                @spawn entity, message
