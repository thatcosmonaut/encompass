current_folder = (...)\gsub('%.[^%.]+$', '') .. '.'

ObjectPool = require(current_folder .. 'object_pool')

class MessagePool extends ObjectPool
    new: (world, message_klass) =>
        super 16, () -> message_klass @
        @world = world

return MessagePool