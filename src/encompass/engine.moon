current_folder = (...)\gsub('%.[^%.]+$', '') .. '.'

utils = require(current_folder .. 'utils')

class Engine
    @component_types: {}

    @assert_component_types: =>
        assert not utils.empty(@@component_types), @@__name .. " cannot have no component types"

    new: (world, ...) =>
        assert not @@frozen, "cannot instantiate frozen class"
        @world = world
        @tracked_entities = {}
        @deactivated_entities = {}

        @initialize ...

    initialize: ->

    subtype_of: (klass) =>
        return @@ == klass or (@@.__parent ~= nil and @@.__parent\subtype_of klass)

    __check_entity: (entity) =>       
        has_all_components = true

        for _, component_type in pairs @@component_types
            has_all_components and= entity\__has_active_component component_type
        
        return has_all_components

    __check_and_track_entity: (entity) =>
        if @__check_entity entity then
            @__track_entity entity

    __check_and_untrack_entity: (entity) =>
        if not @__check_entity entity then
            @__untrack_entity entity

    __track_entity: (entity) =>
        if @tracked_entities[entity] != nil then return
        @tracked_entities[entity] = entity
        entity.tracked_by[@] = @

    __untrack_entity: (entity) =>
        @tracked_entities[entity] = nil
        @deactivated_entities[entity] = nil
        entity.tracked_by[@] = nil

    __activate_entity: (entity) =>
        if @deactivated_entities[entity] != nil then
            @deactivated_entities[entity] = entity
            @tracked_entities[entity] = nil

    __deactivate_entity: (entity) =>
        if @tracked_entities[entity] != nil then
            @deactivated_entities[entity] = entity
            @tracked_entities[entity] = nil

return Engine