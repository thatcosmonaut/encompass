# Encompass

**encompass** is a powerful Hyper ECS framework ideal for game development.

It works best with [LÖVE](https://love2d.org), but it could be integrated
with any game engine that supports Lua scripting. 

## Documentation

Robust documentation for **encompass** is available at Read The Docs: https://encompass.readthedocs.io/en/latest/

## Installation

**encompass** is written in [Moonscript](https://moonscript.org/), a language which compiles to Lua. 

It is recommended to download a compiled release of **encompass** from the [Releases](https://gitlab.com/ehemsley/encompass/releases) page. Then place the downloaded folder in your project and `require` as necessary.

## Building

To build encompass yourself, make sure you have installed `moonscript` through Luarocks. Then run the `build` shell script.

## Testing

To test encompass, make sure you have installed `busted` through Luarocks.
Then run the `test` shell script.

## License

This project is licensed under the Cooperative Software License. 

The long and short of it is that if you are working on behalf of a corporation and not for yourself as an individual or a cooperative, you are not welcome to use this software as-is. If this is the case, please contact [Evan Hemsley](https://gitlab.com/ehemsley) to negotiate an appropriate licensing agreement.