local current_folder = (...):gsub('%.init$', '') .. '.' -- "encompass"

local encompass = {}

encompass.Component = require(current_folder .. 'component')
encompass.DrawComponent = require(current_folder .. 'draw_component')

encompass.Entity = require(current_folder .. 'entity')

encompass.Message = require(current_folder .. 'message')
encompass.ComponentMessage = require(current_folder .. 'messages.component')
encompass.EntityMessage = require(current_folder .. 'messages.entity')
encompass.SideEffectMessage = require(current_folder .. 'messages.side_effect')
encompass.SpawnMessage = require(current_folder .. 'messages.spawn')
encompass.StateMessage = require(current_folder .. 'messages.state')

encompass.Detecter = require(current_folder .. 'engines.detecter')
encompass.Spawner = require(current_folder .. 'engines.processors.spawner')
encompass.PooledSpawner = require(current_folder .. 'engines.processors.pooled_spawner')
encompass.ComponentModifier = require(current_folder .. 'engines.processors.modifiers.component')
encompass.MultiMessageComponentModifier = require(
    current_folder .. 'engines.processors.modifiers.multi_message_component'
)
encompass.EntityModifier = require(current_folder .. 'engines.processors.modifiers.entity')
encompass.Renderer = require(current_folder .. 'engines.renderer')
encompass.SideEffecter = require(current_folder .. 'engines.processors.side_effecter')

encompass.World = require(current_folder .. 'world')

encompass.ObjectPool = require(current_folder .. 'object_pool')

encompass.prof = require(current_folder .. 'lib.jprof')

return encompass