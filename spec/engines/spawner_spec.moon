World = require('encompass.world')
Entity = require('encompass.entity')
Spawner = require('encompass.engines.processors.spawner')
SideEffectMessage = require('encompass.messages.side_effect')
SpawnMessage = require('encompass.messages.spawn')

helper = require('spec.helper')
helper.register(assert)

describe "Spawner", ->
    SpawnMessageA = SpawnMessage\define "SpawnMessageA", a: 'number'

    test_value = 0

    class TestSpawner extends Spawner
        @message_type: SpawnMessageA

        initialize: (init_value) =>
            @init_value = init_value

        spawn: =>
            test_value += 1

    world = World!
    spawner = world\add_spawner TestSpawner, 12

    describe "when added", ->
        it "calls the initialize callback", ->
            assert.are.equal 12, spawner.init_value

    describe "create_entity", ->
        entity = spawner\create_entity!

        it "creates a new entity", ->
            assert.are.same Entity, entity.__class

    describe "when a message of tracked type is created", ->
        message = world\create_message SpawnMessageA, 'a', 5
        world\update(0.01)

        it "calls the spawn method", ->
            assert.are.equal 1, test_value

        it "no longer has message after update", ->
            assert.not.has_value spawner.messages, message

    describe "when defined without message_type", ->
        it "throws an error", ->
            func = ->
                class MySpawner extends Spawner
                    spawn: ->

            assert.error(func, "MySpawner must have a message_type property")

    describe "when defined with incorrect message type", ->
        it "throws an error", ->
            SideEffectMessageA = SideEffectMessage\define "SideEffectMessageA"
            func = ->
                class MySpawner extends Spawner
                    @message_type: SideEffectMessageA
                    spawn: ->

            assert.error(func, "MySpawner must consume a message of SpawnMessage type")

    describe "when defined with no spawn function", ->
        it "throws an error", ->
            func = ->
                class MySpawner extends Spawner
                    @message_type: SpawnMessageA

            assert.error(func, "spawn function must be overridden on MySpawner")