World = require("encompass.world")
Renderer = require("encompass.engines.renderer")
Component = require("encompass.component")
DrawComponent = require("encompass.draw_component")

describe "Renderer", ->
    describe "called by the world draw event", ->
        TestDrawComponent = DrawComponent\define "TestDrawComponent"

        test_value = 1

        class TestRenderer extends Renderer
            @component_types: { TestDrawComponent }

            render: (entity) =>
                test_value += 1

        world = World!
        world\add_renderer TestRenderer

        entity = world\create_entity!
        entity\add_component TestDrawComponent, 'layer', 0

        it "should call the render method", ->
            world\update(0.01)
            world\draw!
            assert.are.equal 2, test_value

    describe "defined with no DrawComponents", ->
        TestComponentA = Component\define "TestComponentA"

        it "throws an error", ->
            func = ->
                class MyRenderer extends Renderer
                    @component_types: { TestComponentA }
                    render: ->

            assert.error(func, "Renderer MyRenderer must track exactly one DrawComponent, was given 0")

    describe "defined with multiple DrawComponents", ->
        DrawComponentA = DrawComponent\define "DrawComponentA"
        DrawComponentB = DrawComponent\define "DrawComponentB"

        it "throws an error", ->
            func = ->
                class MyRenderer extends Renderer
                    @component_types: { DrawComponentA, DrawComponentB }
                    render: ->

            assert.error(func, "Renderer MyRenderer must track exactly one DrawComponent, was given 2")

    describe "defined with no component types", ->
        it "throws an error", ->
            func = ->
                class MyRenderer extends Renderer
                    render: ->

            assert.error(func, "MyRenderer cannot have no component types")

    describe "defined with no render function", ->
        ComponentA = Component\define "ComponentA"

        it "throws an error", ->
            func = ->
                class BrokenRenderer extends Renderer
                    @component_types: { DrawComponent }

            assert.has_error(func, "render must be overridden on BrokenRenderer")