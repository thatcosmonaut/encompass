World = require('encompass.world')
Component = require('encompass.component')
SpawnMessage = require('encompass.messages.spawn')
PooledSpawner = require('encompass.engines.processors.pooled_spawner')

helper = require('spec.helper')
helper.register(assert)

describe "PooledSpawner", ->
    TestComponent = Component\define "TestComponent", a: 'number'
    TestSpawnMessage = SpawnMessage\define "TestSpawnMessage", a: 'number'
    NotTestSpawnMessage = SpawnMessage\define "NotTestSpawnMessage", b: 'number'

    class TestPooledSpawner extends PooledSpawner
        @message_type: TestSpawnMessage
        @pool_size: 5

        initialize: (init_value) =>
            @init_value = init_value
            @other_value = 4

        generate: (pooled_entity) =>
            pooled_entity\add_component TestComponent, 'a', 5

        spawn: =>

    world = World!
    pooled_spawner = world\add_spawner TestPooledSpawner, 15

    describe "when added", ->
        it "calls the initialize callback", ->
            assert.are.equal 15, pooled_spawner.init_value
            assert.are.equal 4, pooled_spawner.other_value

    describe "when a message of tracked type is created", ->
        it "tracks the message", ->
            a_message = world\create_message TestSpawnMessage, 'a', 5
            world\__check_messages!
            assert.has_value pooled_spawner.messages, a_message
            pooled_spawner\__clean!

    describe "when a message not of tracked type is created", ->
        it "does not track the message", ->
            b_message = world\create_message NotTestSpawnMessage, 'b', 5
            world\__check_messages!
            assert.not.has_value pooled_spawner.messages, b_message
            pooled_spawner\__clean!

    describe "generate", ->
        generate_test_value = 0

        class TestGeneratePooledSpawner extends PooledSpawner
            @message_type: TestSpawnMessage
            @pool_size: 5

            generate: (pooled_entity) =>
                generate_test_value += 1

            spawn: =>

        world = World!
        test_spawner = world\add_spawner TestGeneratePooledSpawner

        it "calls generate the correct amount of times", ->
            assert.are.equal 5, generate_test_value

        it "deactivates the generated entities", ->
            world\update(0.01)
            assert.are.equal false, next(test_spawner.object_pool.inactive_objects).active

    describe "spawn", ->
        local test_pooled_entity, test_message

        class TestSpawnPooledSpawner extends PooledSpawner
            @message_type: TestSpawnMessage
            @pool_size: 5

            generate: (pooled_entity) =>
                pooled_entity\add_component TestComponent, 'a', 5

            spawn: (pooled_entity, message) =>
                (pooled_entity\get_component TestComponent).a = message.a
                assert.are.same(test_pooled_entity, pooled_entity)
                assert.are.same(test_message, message)

        world = World!
        test_pooled_spawner = world\add_spawner TestSpawnPooledSpawner
        test_pooled_entity = next(test_pooled_spawner.object_pool.inactive_objects)

        it "is called with the correct arguments", ->
            test_message = world\create_message TestSpawnMessage, 'a', 5
            world\__check_messages!
            world\__spawn!

        it "activates an inactive pooled entity", ->
            assert.true test_pooled_entity.active
            assert.has_value test_pooled_spawner.object_pool.active_objects, test_pooled_entity
            assert.not.has_value test_pooled_spawner.object_pool.inactive_objects, test_pooled_entity

        describe "when pool limit has been reached", ->
            describe "and pooled spawner has expand behavior", ->
                test_world = World!

                class ExpandingPooledSpawner extends PooledSpawner
                    @message_type: TestSpawnMessage
                    @pool_size: 5
                    @overflow_behavior = PooledSpawner.OverflowBehaviors.expand

                    generate: (pooled_entity) =>
                        pooled_entity\add_component TestComponent, "a", 5

                    spawn: (pooled_entity, message) =>
                        component = pooled_entity\get_component TestComponent
                        component.a = message.a_message

                expanding_pooled_spawner = test_world\add_spawner ExpandingPooledSpawner
                test_world\update(0.01)

                for i = 1, 5
                    test_world\create_message TestSpawnMessage, "a", i

                test_world\update(0.01)
                test_world\create_message TestSpawnMessage, "a", 6
                test_world\update(0.01)

                it "should expand the pool count", ->
                    assert.are.equal 10, expanding_pooled_spawner.object_pool.size

                it "should generate a new pooled entity and activate it", ->
                    activated_count = 0
                    for _, _ in pairs expanding_pooled_spawner.object_pool.active_objects
                        activated_count += 1

                    assert.are.equal 6, activated_count

            describe "and pooled spawner has throw behavior", ->
                test_world = World!

                throw_value = false

                class ThrowPooledSpawner extends PooledSpawner
                    @message_type: TestSpawnMessage
                    @pool_size: 5
                    @overflow_behavior = @throw

                    generate: (pooled_entity) =>
                        pooled_entity\add_component TestComponent, "a", 5

                    spawn: (pooled_entity, message) =>
                        component = pooled_entity\get_component TestComponent
                        component.a = message.a

                    throw: (object_pool, pooled_spawner) =>
                        throw_value = true

                test_world\add_spawner ThrowPooledSpawner

                for i = 1, 5
                    test_world\create_message TestSpawnMessage, "a", i

                test_world\update(0.01)
                test_world\create_message TestSpawnMessage, "a", 6
                test_world\update(0.01)

                it "should throw the given function", ->
                    assert.true throw_value

            describe "pooled spawner has fallible behavior", ->
                test_world = World!

                class FalliblePooledSpawner extends PooledSpawner
                    @message_type: TestSpawnMessage
                    @pool_size: 5
                    @overflow_behavior = PooledSpawner.OverflowBehaviors.fallible

                    generate: (pooled_entity) =>
                        pooled_entity\add_component TestComponent, "a", 5

                    spawn: (pooled_entity, message) =>
                        component = pooled_entity\get_component TestComponent
                        component.a = message.a

                fallible_pooled_spawner = test_world\add_spawner FalliblePooledSpawner

                for i = 1, 5
                    test_world\create_message TestSpawnMessage, "a", i

                test_world\update(0.01)
                test_world\create_message TestSpawnMessage, "a", 6
                test_world\update(0.01)

                it "should do nothing", ->
                    assert.are.equal(5, fallible_pooled_spawner.object_pool.size)

                    active_entity_count = 0
                    for _, _ in pairs fallible_pooled_spawner.object_pool.active_objects
                        active_entity_count += 1

                    assert.are.equal(5, active_entity_count)

                    inactive_entity_count = 0
                    for _, _ in pairs fallible_pooled_spawner.object_pool.inactive_objects
                        inactive_entity_count += 1

                    assert.are.equal(0, inactive_entity_count)

            describe "pooled spawner has cycle behavior", ->
                test_world = World!

                class CyclePooledSpawner extends PooledSpawner
                    @message_type: TestSpawnMessage
                    @pool_size: 5
                    @overflow_behavior: PooledSpawner.OverflowBehaviors.cycle

                    generate: (pooled_entity) =>
                        pooled_entity\add_component TestComponent, "a", 5

                    spawn: (pooled_entity, message) =>
                        component = pooled_entity\get_component TestComponent
                        component.a = message.a

                cycle_pooled_spawner = test_world\add_spawner CyclePooledSpawner

                for i = 1, 5
                    test_world\create_message TestSpawnMessage, "a", i

                test_world\update(0.01)
                test_world\create_message TestSpawnMessage, "a", 6
                test_world\update(0.01)

                it "should keep the pool size the same", ->
                    assert.are.equal 5, cycle_pooled_spawner.object_pool.size

                it "should update the properties of an arbitrary active entity", ->
                    result = false
                    for _, entity in pairs cycle_pooled_spawner.object_pool.active_objects
                        if entity\get_component(TestComponent).a == 6 then result = true
                    assert.true result

        describe "when generate function not overridden", ->
            world = World!
            func = ->
                class NotOverriddenPooledSpawner extends PooledSpawner
                    @message_type: TestSpawnMessage
                    @pool_size: 5

                    spawn: =>

            it "throws an error", ->
                assert.has_error func, "NotOverriddenPooledSpawner must implement generate function"

        describe "when spawn function not overridden", ->
            world = World!
            func = ->
                class NotOverriddenPooledSpawner extends PooledSpawner
                    @message_type: TestSpawnMessage
                    @pool_size: 5

                    generate: =>

            it "throws an error", ->
                assert.has_error func, "NotOverriddenPooledSpawner must implement spawn function"

        describe "when no message type is given", ->
            world = World!
            func = ->
                class BrokenPooledSpawner extends PooledSpawner
                    @pool_size: 5

                    generate: =>
                    spawn: =>

            it "throws an error", ->
                assert.has_error func, "BrokenPooledSpawner must have a message_type property"

        describe "when no pool count is given", ->
            world = World!
            func = ->
                class BrokenPooledSpawner extends PooledSpawner
                    @message_type: TestSpawnMessage

                    generate: =>
                    spawn: =>

            it "throws an error", ->
                assert.has_error func, "BrokenPooledSpawner must have a pool_size"