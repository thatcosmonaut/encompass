World = require('encompass.world')
MultiMessageComponentModifier = require('encompass.engines.processors.modifiers.multi_message_component')

Component = require('encompass.component')
ComponentMessage = require('encompass.messages.component')
EntityMessage = require('encompass.messages.entity')
StateMessage = require('encompass.messages.state')

helper = require('spec.helper')
helper.register(assert)

describe "MultiMessageComponentModifier", ->
    ComponentA = Component\define "ComponentA", a: "number"
    MessageA = ComponentMessage\define "MessageA", component: ComponentA

    ComponentB = Component\define "ComponentB", b: "number"
    MessageB = ComponentMessage\define "MessageB", component: ComponentB

    describe "when a message is created", ->
        class TestModifier extends MultiMessageComponentModifier
            @message_type: MessageA
            modify: =>

        world = World!
        modifier = world\add_modifier TestModifier

        entity = world\create_entity!
        a_component = entity\add_component ComponentA, "a", 4

        b_entity = world\create_entity!
        b_component = b_entity\add_component ComponentB, "b", 3

        describe "of correct type", ->
            a_message = world\create_message MessageA, "component", a_component
            world\__check_messages!

            it "tracks the message", ->
                assert.has_value modifier.component_to_messages[a_component], a_message
                modifier\__clean!

        describe "of incorrect type", ->
            world\create_message MessageB, "component", b_component
            world\__check_messages!

            it "does not track the message", ->
                assert.nil modifier.component_to_messages[b_component]
                modifier\__clean!

    describe "create_entity", ->
        world = World!

        class TestModifier extends MultiMessageComponentModifier
            @message_type: MessageA
            modify: =>

        test_modifier = world\add_modifier TestModifier

        it "creates entity and adds it to world", ->
            entity = test_modifier\create_entity!
            assert.has_value world.entities, entity

    describe "modify", ->
        TestComponent = Component\define "TestComponent", a: "number"
        TestMessage = ComponentMessage\define "TestMessage", component: TestComponent

        local test_component, test_message_one, test_message_two

        class TestMultiMessageComponentModifier extends MultiMessageComponentModifier
            @message_type: TestMessage

            modify: (component, frozen_fields, messages, dt) =>
                assert.are.same test_component, component
                assert.are.equal 3, frozen_fields.a
                assert.has_value messages, test_message_one
                assert.has_value messages, test_message_two
                assert.are.equal 0.01, dt

        world = World!

        world\add_modifier TestMultiMessageComponentModifier

        entity = world\create_entity!
        test_component = entity\add_component TestComponent, "a", 3
        test_message_one = world\create_message TestMessage, "component", test_component
        test_message_two = world\create_message TestMessage, "component", test_component

        it "is called with correct arguments", ->
            world\__check_messages!
            world\__modify 0.01

    describe "state components", ->
        TestMessage = ComponentMessage\define "TestMessage"
        TestStateMessageA = StateMessage\define "TestStateMessageA", value: "number"
        TestStateMessageB = StateMessage\define "TestStateMessageB", value: "number"

        class TestStateMessageComponentModifier extends MultiMessageComponentModifier
            @message_type: TestMessage
            @state_message_types: { TestStateMessageA, TestStateMessageB }

            modify: =>

        world = World!
        modifier = world\add_modifier TestStateMessageComponentModifier

        it "is stored on the processor", ->
            test_state_message_a = world\create_message TestStateMessageA, "value", 2
            test_state_message_b = world\create_message TestStateMessageB, "value", 3
            world\__check_messages!
            world\__check_state_messages!
            assert.are.same test_state_message_a, modifier\get_state_message(TestStateMessageA)
            assert.are.same test_state_message_b, modifier\get_state_message(TestStateMessageB)
            assert.are.equal 2, test_state_message_a.value
            assert.are.equal 3, test_state_message_b.value

        it "is cleared after world processes messages", ->
            test_state_message_a = world\create_message TestStateMessageA, "value", 2
            test_state_message_b = world\create_message TestStateMessageB, "value", 3
            world\__modify 0.01
            assert.is_not.has_value modifier.state_components, test_state_message_a
            assert.is_not.has_value modifier.state_components, test_state_message_b

    describe "modify not overriden", ->
        func = ->
            class NotOverriddenModifier extends MultiMessageComponentModifier
                @message_type: MessageA

        it "throws an error", ->
            assert.has_error func, "NotOverriddenModifier must implement modify function"

    describe "defined with no message type", ->
        func = ->
            class NoMessageTypeModifier extends MultiMessageComponentModifier
                modify: =>

        it "throws an error", ->
            assert.has_error func, "NoMessageTypeModifier must have a message_type property"

    describe "defined with incorrect message type", ->
        TestEntityMessage = EntityMessage\define "TestEntityMessage"

        func = ->
            class IncorrectMessageTypeModifier extends MultiMessageComponentModifier
                @message_type: TestEntityMessage
                modify: =>

        it "throws an error", ->
            assert.has_error func, "IncorrectMessageTypeModifier must consume messages of ComponentMessage type"