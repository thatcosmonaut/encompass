World = require('encompass.world')
SideEffecter = require('encompass.engines.processors.side_effecter')
SideEffectMessage = require('encompass.messages.side_effect')

helper = require('spec.helper')
helper.register(assert)

describe "SideEffecter", ->
    SideEffectMessageA = SideEffectMessage\define "SideEffectMessageA", a: "number"

    test_value = 0

    class TestSideEffecter extends SideEffecter
        @message_type: SideEffectMessageA

        initialize: (init_value) =>
            @init_value = init_value

        effect: =>
            test_value += 1

    world = World!
    side_effecter = world\add_side_effecter TestSideEffecter, 26

    describe "when added", ->
        it "calls the initialize callback", ->
            assert.are.equal 26, side_effecter.init_value

    describe "when a message of tracked type is created", ->
        message = world\create_message SideEffectMessageA, "a", 5
        world\update 0.01

        it "calls the effect method", ->
            assert.are.equal 1, test_value

        it "no longer has message after update", ->
            assert.not.has_value side_effecter.messages, message

    describe "when defined without message_type", ->
        it "throws an error", ->
            func = ->
                class MySideEffecter extends SideEffecter
                    effect: ->

            assert.error(func, "MySideEffecter must have a message_type property")

    describe "when defined without effect function", ->
        it "throws an error", ->
            func = ->
                class MySideEffecter extends SideEffecter
                    @message_type: SideEffectMessageA

            assert.error(func, "effect function must be overridden on MySideEffecter")