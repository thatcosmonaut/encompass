.. _spawner:

Spawner
=======

::

    local Spawner = require("encompass").Spawner

A Spawner is an Engine which is responsible for reading
a :ref:`SpawnMessage <spawn_message>` to create an :ref:`Entity <entity>`.

Spawners are defined by the Message prototype they track,
and the ``spawn`` function they implement.

It is an anti-pattern to read Entities or Components
from inside a Spawner. *Do not do this.*

Function Reference
------------------

.. function:: Spawner.define(name, message_type)

    :param string name: The name of the Spawner.
    :param prototype message_type: A Message prototype that should be tracked by the Spawner.

Defines a Spawner that will track the given Message prototype.

.. function:: Spawner:initialize(...)

    :param args ...: An arbitrary list of arguments.

This callback is triggered when the Spawner is added to the World.
Useful for Spawners that need to deal with side effects, such as
physics worlds or particle systems.

.. function:: Spawner:create_entity()

    :returns: ``Entity`` An instantiated entity.

.. function:: Spawner:spawn(message)

    :param SpawnMessage message: A reference to a message that has been tracked by the Spawner.

This callback is triggered when a Message of the specified prototype is produced by a Detecter.
The programmer must override this function or an error will be thrown.

Example
-------

::

    local SoundComponent = require('game.components.sound')
    local SoundSpawnMessage = require('game.messages.spawners.sound')

    local Spawner = require('encompass').Spawner
    local SoundSpawner = Spawner.define('SoundSpawner', SoundSpawnMessage)

    function SoundSpawner:spawn(sound_spawn_message)
        local source = sound_spawn_message.source

        local sound_entity = self:create_entity()
        sound_entity:add_component(SoundComponent, {
            source = source
        })

        source:play()
    end

    return SoundSpawner