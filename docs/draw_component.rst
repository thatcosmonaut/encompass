.. _draw_component:

DrawComponent
=============

::

    local DrawComponent = require('encompass').DrawComponent

A DrawComponent is a special kind of :ref:`Component <component>` intended for use by Renderers.

The only difference is that it implicitly contains a ``layer`` property
so it can be ordered properly by the draw system.

It is very expensive to modify the ``layer`` property at runtime.
Do so sparingly.

Example
-------

::

    local DrawComponent = require('encompass').DrawComponent

    local DrawCanvasComponent = DrawComponent.define('DrawCanvasComponent', {
        canvas = 'userdata',
        w = 'number',
        h = 'number'
    })

    entity:add_component(DrawCanvasComponent, {
        canvas = love.graphics.newCanvas(1280, 720)
        w = 1280,
        h = 720,
        layer = -5
    })