.. _field_type_reference:

Field Type Reference
====================

The following is a list of valid types
which can be specified for :ref:`Components <components>`
and :ref:`Messages <messages>`.

* ``'boolean'``
* ``'number'``
* ``'string'``
* ``'userdata'``
* ``'table'``
* ``Component``
* ``Entity``
* any user-defined ``Component`` prototype