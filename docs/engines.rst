Engines
=======

An Engine is the **encompass** notion of a System.

There are two major types of Engines.

Detectors and Renderers are responsible for iterating over :ref:`Entities <entities>` that contain
particular components and producing Messages in response.
They do not modify Entities or Components directly.

Spawners and Modifiers are responsible for consuming Messages and
modifying the game state in response. They do not read from Entities or Components.

.. toctree::
    :maxdepth: 2

    Detecter <detecter>
    Spawner <spawner>
    PooledSpawner <pooled_spawner>
    ComponentModifier <component_modifier>
    MultiMessageComponentModifier <multi_message_component_modifier>
    EntityModifier <entity_modifier>
    Renderer <renderer>
    SideEffecter <side_effecter>
