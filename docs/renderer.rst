Renderer
========

::

    local Renderer = require('encompass').Renderer

A Renderer is an Engine which is responsible for reading
components to draw elements to the screen.

Renderers are defined by the :ref:`Component <component>` prototype(s) they
track, of which *exactly one* must be a :ref:`DrawComponent <draw_component>`,
and the ``render`` function they implement.

It is an anti-pattern to modify Entities or Components from inside
a Renderer. *Do not do this.*

Function Reference
------------------

.. function:: Renderer.define(name, component_types)

    :param string name: The name of the Renderer
    :param table component_types:
        An array-style table containing Components, *exactly one* of
        which must be a DrawComponent. If this is not the case, an error
        will be thrown.

Defines a Renderer that will track Entities which contain *all*
of the given component types.

.. function:: Renderer:initialize(...)

    :param args ...: An arbitrary list of arguments.

This callback is triggered when the Renderer is added to the World.

Useful for dealing with side effects, like initializing framebuffers.

.. function:: Renderer:render(entity, canvas)

    :param Entity entity: A reference to an entity which is tracked by the Renderer
    :param Canvas canvas:
        A reference to the current canvas (framebuffer)

Every frame, this callback runs for each entity tracked by the Renderer.
The programmer must override this callback or an error will be thrown.