.. _side_effecter:

SideEffecter
============

::

    local SideEffecter = require('encompass').SideEffecter

A SideEffecter is an Engine which is responsible
for consuming :ref:`SideEffectMessages <side_effect_message>`
and producing side effects in response. For example,
if your game has multiple top-level states with separate Worlds,
changing the top-level state would be a side effect.

Side effects should be used as an absolute last resort.
Use them only if there is no way to produce the behavior with
other types of Engines.

Using a SideEffecter to modify Entities or Components
is an anti-pattern. *Do not do this.*

Function Reference
------------------

.. function:: SideEffecter.define(name, message_type)

    :param string name: The name of the SideEffecter.
    :param prototype message_type: An SideEffecter prototype that should be tracked by the SideEffecter.

.. function:: SideEffecter:effect(message)

    :param SideEffectMessage message: A reference to a SideEffectMessage that has been tracked by the SideEffecter.

This callback is triggered when a SideEffectMessage of the specified prototype is produced.
The programmer must override this function or an error will be thrown.